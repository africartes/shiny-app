# Tout Afrique

Lien de l'application : [https://rconnect.afd.fr/ToutAfrique/](https://rconnect.afd.fr/ToutAfrique/)
# Sommaire
1. Création de la base de données
    - Récupération des fichiers Excel
    - Récupération des données via l'API DBnomics
    - base finale
2. Shapefiles
    - Carte vision Mondiale
    - Carte Afrique vision DR
    - Carte Afrique vision pays
3. Application
    - structure
    - 
    - 

## 1. Création de la base de données

Les informations sur les données sont détaillées dans le fichier Excel "Résumé Indicateurs"

### 1.1. Récupération des fichiers Excel

Certains fichiers ne peuvent pas être récupérés via des APIs. Il est nécessaire de les importer à la main puis de leur appliquer un traitement afin qu'ils soient harmonisés. 
Ces fichiers sont:
* [IDH](http://hdr.undp.org/en/data)
* [Fragilité politique](https://datacatalog.worldbank.org/dataset/harmonized-list-fragility-conflict-and-violence-countries)
* [Liberté dans le monde](https://freedomhouse.org/content/freedom-world-data-and-resources)
* [Régimes politiques](https://ourworldindata.org/democracy)
* [Vulnérabilité climatique FERDI](http://www.ferdi.fr/en/indicator/index-physical-vulnerability-climate-change)
* [Vulnérabilité climatique Ndgain](https://gain.nd.edu/our-work/country-index/download-data/)
* [Incubateurs de start-up](https://www.theatlas.com/charts/r1iAH50FG)

### 1.2. Récupération des données via l'API DBnomics

Tous les autres indicateurs sont facilement récupérables via des APIs et notamment celles de [DBnomics](https://db.nomics.world/).
Le package rdbnomics permet de se connecter aux API et de récupérer les indicateurs dont on a besoin. Ces indicateurs ne proviennent pas tous du même provider/fournisseur. Certains dépendent de la World Bank, d'autres du FMI ou encore de UNdata. Il est donc nécessaire d'adapter la fonction rdb du package rdbnomics à ces différents fournisseurs. 
<br> __Exemple__ :
* _Fournisseur WB WDI_ : 
```
rdb(  "WB", "WDI" , dimensions = '{"indicator":["SE.ADT.LITR.ZS", "SE.ADT.1524.LT.ZS", "NY.GDP.MKTP.KD.ZG"]}' )
```
* _Fournisseur FMI WEO_ :
```
rdb( "IMF", "WEO" , dimensions = '{"weo-subject":["GGR_NGDP", "NGDPD", "NGDPDPC"]}' )
```
* _Fournisseur UNdata_ :
```
rdb( "UNDATA", "DF_UNDATA_WPP" , dimensions = '{"INDICATOR":["SP_POP_TOTL"], "AGE" : ["_T"], "SEX" : ["_T"], "SCENARIO" : ["M"]}' )
```

Ces indicateurs nécessitent également d'être harmonisés grâce aux fonctions :
```diff 
+ traitement_base
+ traitement_baseUNDATA
```
### 1.3. Base finale

La premier étape consiste à joindre tous les indicateurs préalablement harmonisés en une seule et même base de données puis de joindre celle-ci avec la base des correspondances.

#### 1.3.1. Pondération DR

Afin de prendre en compte la vision DR, il est nécessaire de pondérer certains indicateurs. 
<br>La croissance du PIB est pondérée par le PIB en PPA
<br>La recette budgétaire est pondérée par le PIB en prix courant.
<br>La plupart des indicateurs socio-économiques sont pondérés par la population
<br>D'autres indicateurs sont sommés ou moyennés.
<br>D'autres encore ne subissent aucune transformation.

Les fonctions qui permettent d'appliquer ces traitements sur les indicateurs sont :
```diff 
+ ponderation
+ ponderSumMean
```

#### 1.3.2. Échelles

Afin d'assurer la pertinence des visualisations, il est très important d'adopter les bonnes échelles.
<br> Pour cela, nous avons tracer les quantiles de chaque indicateurs et avons créer des classes.
<br> Le but de l'exercice n'était pas de créer les classes les plus précises possibles au dizième près mais plutôt d'utiliser notre bon sens pour fournir le découpage qui garantisse la meilleure visualisation/compréhension.

## 2. Shapefiles

Posséder de bons fonds de cartes relève à la fois d'enjeux en termes de visualisation mais aussi et surtout d'enjeux diplomatiques. 
<br> En effet, l'AFD doit s'assurer de ne pas créer de conflits diplomatiques avec les pays dans lesquels elle intervient.

### 2.1. Carte vision Mondiale

Le fond de carte pour la vision mondiale est récupéré via la fontion **getMap**.
```
MONDE <- getMap(resolution = "coarse")  # differentes resolutions disponibles mais seule coarse permet de prendre en compte la frontière maroc - sahara occidental
MONDE <- subset(MONDE, !MONDE$ISO3 %in% "ATA") # On supprime l'Antartique
```

### 2.2. Carte Afrique vision pays

Le fond de carte pour la vision pays se base sur la carte du monde. Nous avons uniquement conservé les pays pour lesquels il existe une clé commune avec la base AFR.
Le fond de carte est donc le ressemblement du continent Africain avec la DR Océan Indien.

```
africa <- subset(MONDE, MONDE$ISO3  %in%  c(unique(AFR$iso3), "ESH"))
```

### 2.3. Carte Afrique vision DR

La carte de la vision DR est basé sur la vision Pays.
<br> Nous avons simplement joint le shapefile de la vision pays avec les identifiants correspondants aux différentes DRs.
```
id <- jointure_carte_pays$ADMIN
liste_Afrique_australe <- c("Angola", "Botswana",  "Lesotho", "Malawi", "Mozambique", "Namibia", "South Africa", "Swaziland", "Zambia", "Zimbabwe")
id[id %in% liste_Afrique_australe]    <-  "Afrique Australe"
simplifyPoly                          <-  gSimplify(jointure_carte_pays, tol = .00001)
withoutBadPoly                        <-  gBuffer(simplifyPoly, byid=TRUE, width=.1) # supprime les petits défauts sur la carte
afrique_australe                      <-  unionSpatialPolygons(withoutBadPoly, id)
```
<br>Attention ! Pour cette partie il est nécessaire de fournir une projection plane.
```
proj4string(africa) <- CRS("+init=epsg:3347")
africa <- spTransform(africa, CRS( "+init=epsg:3347" ))
```

