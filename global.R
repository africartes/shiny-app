#-------------------------------------------                                                       Importation des packages                                                            -------------------------------------------#  ====   


library(shiny)
library(shinyjs)
library(shinydashboard)
library(shinydashboardPlus)
library(shinyWidgets)#prettyRadioButtons
library(shinycssloaders) #withSpinner
library(data.table)
library(leaflet)
library(stringr) #str_detect
library(dplyr)
library(htmlwidgets) #OnRender pour intégrer du JS
library(R.utils) #chargement des fonctions
library(RColorBrewer) 
library(plotly)
library(treemap)

sourceDirectory("utils")


#-------------------------------------------                                                      Chargement des bases de donnees                                                      -------------------------------------------#  ====   


# Base de donnees de tous les codes pays Afrique :
AFR <- readRDS("BASE/bases_finales/base_Afrique_carte.rds")
AFR <- AFR %>% setDT()
AFR[Echelle == "", Echelle := NA]
AFR$value <- round(AFR$value + 0.000000000000000001, 2)

# Importation du fond de carte du continent Africain
africa <- readRDS(file = "shapefiles/shape_AFR.rds")  

# Importation du fond de carte du continent Africain
africaDR <- readRDS(file = "shapefiles/shape_AFR_DR.rds") 
africaDR <- readRDS(file = "shapefiles/shape_AFR_DR.rds") 
# Fond de carte du monde
MONDE <- readRDS(file = "shapefiles/MONDE.rds") 

# Chargement de la base de donnees de correspondance pour la légende et la couleur des graphiques :
correspondance <- readRDS(file = "BASE/Data/corres.rds")
correspondance <- correspondance[-nrow(correspondance),]  # suppression de la dernière ligne de la table des correspondances   

# Chargement du fichier des DR
dr <- readRDS("BASE/bases_finales/base_Afrique_DR.rds")
dr <- dr %>% setDT()
dr[Echelle == "", Echelle := NA]
dr$value <- round(dr$value + 0.000000000000000001, 2)

# Chargement de la base de donnees des indicateurs mondiaux :
base_mondiale       <- readRDS("BASE/bases_finales/carte_mondiale.rds") 
base_mondiale <- base_mondiale %>% setDT()
base_mondiale[ Echelle == "",  Echelle := NA]
base_mondiale$value <- round(base_mondiale$value + 0.000000000000000001, 2)

# Chargement de la base de donnees de la pyramide des ages :
pyramide <- readRDS("BASE/bases_finales/pyramide.rds")
pyramide <- pyramide %>% setDT()

#Listes des indicateurs 

listeIndicAfriquePluriel       = c( "PIB" , "PIB par habitant" , "Croissance du PIB" , "Recettes budgétaires", 'IDH', 
                                    "Alphabétisation des jeunes",  "Alphabétisation des adultes" )

listeIndicAfriqueEnTransitions = c( "Population totale" , "Population rurale", "Population vivant dans la pauvreté", 
                                    "Mortalité infantile" , "Sous-nutrition" )

listeIndicLesTransitions       = c("Rendement céréalier", "Eau douce dans l'agriculture" , "Emploi dans l'agriculture",
                                   'Bidonvilles',  "Taux d'urbanisation" = "Population urbaine",
                                   "Accès à l'électricité" , "Production d'électricité renouvelable",
                                   "Abonnements mobiles" , "Titulaire d'un compte, jeunes" = "Titulaire d'un compte, jeunes adultes", "Titulaire d'un compte, adultes",
                                   "Vulnérabilité climatique Ndgain" , "Vulnérabilité climatique FERDI",
                                   "Liberté dans le monde" , "Fragilité politique", "Régimes politiques")

listeIndicSixDefisARelever     = c("Migration", "Incubateurs de start-up" ,
                                   "Écoles primaires - Eau potable" = "Eau potable - Écoles primaires",  
                                   "Écoles primaires - Électricité"="Accès électricité - Écoles primaires", 
                                   "Écoles primaires - Sanitaires" = "Installations sanitaires non mixtes - Écoles primaires",
                                   "Enseignants formés - primaire", "Enseignants formés - secondaire",
                                   "Volume Import - biens","Volume Export - biens","Volume Import - biens et services","Volume Export - biens et services")

anneeActuelle = lubridate::year(Sys.time())


listeAnneePyramide <- unique(pyramide[,annee])


liste_pays_afr   <- as.data.table(unique(AFR$pays))           %>% setnames("V1","pays")
liste_pays_monde <- as.data.table(unique(base_mondiale$pays)) %>% setnames("V1","pays")
listeDR   <- unique(pyramide[,DR])

DF_DR_PAYS = pyramide[ order(DR, pays), .(DR, pays)] %>% unique()

DF_DR_ANNEES = pyramide[ order(DR, annee), .(DR, annee)] %>% unique()

#------------------------------------              Option pour initialiser la valeur par défault du selectizeInput                     -------------------------------------# ====


options_default = list( placeholder = 'A définir', onInitialize = I('function() { this.setValue(""); }'))


#-------------------------------------------                                           Paramètres carte                                             -------------------------------------------#  ====   

tag.map.title <- tags$style(HTML(".leaflet-control.map-title { 
                                 padding-left: 10px;
                                 font-weight: bold;
                                 font-size: 21px;
                                 }")
)


tag.map.title.world <- tags$style(HTML(".leaflet-control.map-title-world { 
                                       padding-left: 10px;
                                       font-weight: bold;
                                       font-size: 25px;
                                       }")
 )

#unique(AFR$Indicateur_raccourci)

#-------------------------------------------                     Définitions des cuts et de leur ordre pour vision pays                             -------------------------------------------#  ====

levels_acces_elec              <- unique(AFR[Indicateur_raccourci %in% "acces_elec"                     , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA)) ])
levels_eau_douce_agri          <- unique(AFR[Indicateur_raccourci %in% "eau_douce_agri"                 , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA)) ])
levels_bidonvilles             <- unique(AFR[Indicateur_raccourci %in% "bidonvilles"                    , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA)) ])
levels_emploi_agri             <- unique(AFR[Indicateur_raccourci %in% "emploi_agri"                    , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA)) ])
levels_prod_elec_renouv        <- unique(AFR[Indicateur_raccourci %in% "prod_elec_renouv"               , factor(Echelle, levels = c('< 0.25', '0.25 - 0.5', '0.5 - 1','1 - 2','2 - 5','> 5', NA)) ])
levels_enseignant_primaire     <- unique(AFR[Indicateur_raccourci %in% "enseignant_primaire"            , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA)) ])
levels_enseignant_secondaire   <- unique(AFR[Indicateur_raccourci %in% "enseignant_secondaire"          , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA)) ])
levels_acces_elec_primaire     <- unique(AFR[Indicateur_raccourci %in% "acces_elec_primaire"            , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA)) ])
levels_volume_exp_biens        <- unique(AFR[Indicateur_raccourci %in% "volume_exp_biens"               , factor(Echelle, levels = c('< -10', '-10 - 0', '0 - 3','3 - 6','6 - 9','9 - 12','> 12', NA)) ])
levels_volume_exp_bs           <- unique(AFR[Indicateur_raccourci %in% "volume_exp_bs"                  , factor(Echelle, levels = c('< -10', '-10 - 0', '0 - 3','3 - 6','6 - 9','9 - 12','> 12', NA)) ])
levels_volume_import_biens     <- unique(AFR[Indicateur_raccourci %in% "volume_import_biens"            , factor(Echelle, levels = c('< -10', '-10 - 0', '0 - 3','3 - 6','6 - 9','9 - 12','> 12', NA)) ])
levels_volume_import_bs        <- unique(AFR[Indicateur_raccourci %in% "volume_import_bs"               , factor(Echelle, levels = c('< -10', '-10 - 0', '0 - 3','3 - 6','6 - 9','9 - 12','> 12', 'NA')) ])
levels_eau_potable_primaire    <- unique(AFR[Indicateur_raccourci %in% "eau_potable_primaire"           , factor(Echelle, levels = c('< 40', '40 - 60', '60 - 80','80 - 90','90 - 100' , NA)) ])
levels_sanitaire_primaire      <- unique(AFR[Indicateur_raccourci %in% "sanitaire_primaire"             , factor(Echelle, levels = c('< 40', '40 - 60', '60 - 80','80 - 90','90 - 100' , NA)) ])
levels_alpha_jeune             <- unique(AFR[Indicateur_raccourci %in% "alpha_jeune"                    , factor(Echelle, levels = c('< 30', '30 - 40', '40 - 50','50 - 60','60 - 70','70 - 80','> 80', NA)) ])
levels_alpha_adulte            <- unique(AFR[Indicateur_raccourci %in% "alpha_adulte"                   , factor(Echelle, levels = c('< 30', '30 - 40', '40 - 50','50 - 60','60 - 70','70 - 80','> 80', NA)) ]) 
levels_croissance_pib          <- unique(AFR[Indicateur_raccourci %in% "croissance_pib"                 , factor(Echelle, levels = c('< 0','0 - 2', '2 - 4', '4 - 6', '6 - 8', '> 8', NA))] ) 
levels_pib_hab                 <- unique(AFR[Indicateur_raccourci %in% "pib_hab"                        , factor(Echelle, levels = c('< 200','200  - 400', '400  - 600', '600  - 1000', '1000 - 1500', '> 1500', NA))] ) 
levels_pib                     <- unique(AFR[Indicateur_raccourci %in% "pib"                            , factor(Echelle, levels = c('< 1','1 - 2', '2 - 5', '5 - 10', '10 - 20', '> 20', NA))] ) 
levels_idh                     <- unique(AFR[Indicateur_raccourci %in% "idh"                            , factor(Echelle, levels = c('< 0.4','0.5' , '0.6', '0.7', '> 0.7', NA))] ) 
levels_recette_budg            <- unique(AFR[Indicateur_raccourci %in% "recette_budg"                   , factor(Echelle, levels = c('< 10','10 - 15', '15 - 20', '20 - 25', '25 - 30', '> 30', NA))] ) 
levels_pop_pauvrete            <- unique(AFR[Indicateur_raccourci %in% "pop_pauvrete"                   , factor(Echelle, levels = c('< 10','10 - 20', '20 - 30', '30 - 40', '40 - 50', '> 50', NA))] ) 
levels_mortal_infant           <- unique(AFR[Indicateur_raccourci %in% "mortal_infant"                  , factor(Echelle, levels = c('< 0.05','0.05 - 0.10', '0.10 - 0.15', '0.15 - 0.20', '0.20 - 0.25', '> 0.25' , NA))] ) 
levels_pop_tot                 <- unique(AFR[Indicateur_raccourci %in% "pop_tot"                        , factor(Echelle, levels = c('< 0.5','0.5 - 5', '5 - 20', '20 - 50', '50 - 100', '> 100', NA))] ) 
levels_sous_nutri              <- unique(AFR[Indicateur_raccourci %in% "sous_nutri"                     , factor(Echelle, levels = c('< 10','10 - 15', '15 - 20', '20 - 25', '25 - 30', '> 30', NA))] ) 
levels_pop_rurale              <- unique(AFR[Indicateur_raccourci %in% "pop_rurale"                     , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA))] ) 
levels_compte_adulte           <- unique(AFR[Indicateur_raccourci %in% "compte_adulte"                  , factor(Echelle, levels = c('< 10','10 - 20', '20 - 30', '30 - 40', '40 - 50', '> 50', NA))] )
levels_compte_jeune            <- unique(AFR[Indicateur_raccourci %in% "compte_jeune"                   , factor(Echelle, levels = c('< 10','10 - 20', '20 - 30', '30 - 40', '> 40', NA))] ) 
levels_rdt_cerealier           <- unique(AFR[Indicateur_raccourci %in% "rdt_cerealier"                  , factor(Echelle, levels = c('< 500','500 - 750', '750 - 1000', '1000 - 1250', '1250 - 1500', '> 1500', NA))] ) 
levels_abo_mobile              <- unique(AFR[Indicateur_raccourci %in% "abo_mobile"                     , factor(Echelle, levels = c('< 1','1 - 5', '5 - 10', '10 - 15', '> 15', NA))] ) 
levels_vuln_clim_ferdi         <- unique(AFR[Indicateur_raccourci %in% "vuln_clim_ferdi"                , factor(Echelle, levels = c('< 50','50 - 52', '52 - 56', '56 - 58', '58 - 60', '> 60', NA))] )
levels_vuln_clim_ndgain        <- unique(AFR[Indicateur_raccourci %in% "vuln_clim_ndgain"               , factor(Echelle, levels = c('< 0.45','0.45 - 0.5', '0.5 - 0.55', '0.55 - 0.6', '> 0.6', NA))] ) 
levels_pop_urbaine             <- unique(AFR[Indicateur_raccourci %in% "pop_urbaine"                    , factor(Echelle, levels = c('< 20', '20 - 40', '40 - 60','60 - 80','80 - 100', NA))] ) 
levels_incub_start_up          <- unique(AFR[Indicateur_raccourci %in% "incub_start_up"                 , factor(Echelle, levels = c('< 15','15 - 20', '20 - 25', '25 - 30','> 30', NA))] ) 
levels_migration               <- unique(AFR[Indicateur_raccourci %in% "migration"                      , factor(Echelle, levels = c('< -2','-2 - -0.5', '-0.5 - 0', '0 - 1', '> 1', NA))] ) 
#levels_eau_potable             <- unique(AFR[Indicateur_raccourci %in% "eau_potable"                    , factor(Echelle, levels = c('< 10','10 - 20', '20 - 30', '30 - 40', '40 - 50', '50 - 60', '> 60', NA))] )
levels_lib_monde               <- unique(AFR[Indicateur_raccourci %in% "lib_monde"                      , factor(Echelle, levels = c('Non libre', 'Partiellement libre', 'Libre', NA))] ) 
levels_frag_pol                <- unique(AFR[Indicateur_raccourci %in% "frag_pol"                       , factor(Echelle, levels = c('Pas de fragilité politique', 'Fragilité politique', NA) )] ) 
levels_regime_pol              <- unique(AFR[Indicateur_raccourci %in% "regime_pol"                     , factor(Echelle, levels = c('Colonie','Autocratie', 'Anocratie fermée', 'Anocratie ouverte', 'Démocratie imparfaite', 'Démocratie', NA))] ) 


#-------------------------------------------                     Définitions des cuts et de leur ordre pour vision DR                             -------------------------------------------#  ====


levels_acces_elec_DR           <- unique(dr[Indicateur_raccourci %in% "acces_elec"                     , factor(Echelle, levels = c('< 20', '20 - 30', '30 - 40','40 - 50','> 50', NA)) ])
levels_eau_douce_agri_DR       <- unique(dr[Indicateur_raccourci %in% "eau_douce_agri"                 , factor(Echelle, levels = c('< 60', '60 - 70', '70 - 80','80 - 90','90 - 100', NA)) ])
levels_bidonvilles_DR          <- unique(dr[Indicateur_raccourci %in% "bidonvilles"                    , factor(Echelle, levels = c('< 50', '50 - 60', '60 - 70','70 - 80','> 80', NA)) ])
levels_emploi_agri_DR          <- unique(dr[Indicateur_raccourci %in% "emploi_agri"                    , factor(Echelle, levels = c('< 40', '40 - 50', '50 - 60','60 - 70','> 70', NA)) ])
levels_prod_elec_renouv_DR     <- unique(dr[Indicateur_raccourci %in% "prod_elec_renouv"               , factor(Echelle, levels = c('< 0.25', '0.25 - 0.5', '0.5 - 1','1 - 2','2 - 5','5 - 10','> 10', NA)) ])
levels_enseignant_primaire_DR  <- unique(dr[Indicateur_raccourci %in% "enseignant_primaire"            , factor(Echelle, levels = c('< 60', '60 - 70', '70 - 80','80 - 90','> 90', NA)) ])
levels_enseignant_secondaire_DR<- unique(dr[Indicateur_raccourci %in% "enseignant_secondaire"          , factor(Echelle, levels = c('< 50', '50 - 60', '60 - 70','70 - 80','> 80', NA)) ])
levels_acces_elec_primaire_DR  <- unique(dr[Indicateur_raccourci %in% "acces_elec_primaire"            , factor(Echelle, levels = c('< 20', '20 - 30', '30 - 40','> 40', NA)) ])
levels_volume_exp_biens_DR     <- unique(dr[Indicateur_raccourci %in% "volume_exp_biens"               , factor(Echelle, levels = c('< 0', '0 - 3','3 - 6','6 - 9','9 - 12','> 12', NA)) ])
levels_volume_exp_bs_DR        <- unique(dr[Indicateur_raccourci %in% "volume_exp_bs"                  , factor(Echelle, levels = c('< 0', '0 - 3','3 - 6','6 - 9','9 - 12','> 12', NA)) ])
levels_volume_import_biens_DR  <- unique(dr[Indicateur_raccourci %in% "volume_import_biens"            , factor(Echelle, levels = c('< 0', '0 - 3','3 - 6','6 - 9','9 - 12','> 12', NA)) ])
levels_volume_import_bs_DR     <- unique(dr[Indicateur_raccourci %in% "volume_import_bs"               , factor(Echelle, levels = c('< 0', '0 - 3','3 - 6','6 - 9','9 - 12','> 12', NA)) ])
levels_eau_potable_primaire_DR <- unique(dr[Indicateur_raccourci %in% "eau_potable_primaire"           , factor(Echelle, levels = c('< 40', '40 - 60', '60 - 80','> 80' , NA)) ])
levels_sanitaire_primaire_DR   <- unique(dr[Indicateur_raccourci %in% "sanitaire_primaire"             , factor(Echelle, levels = c('< 50', '50 - 70', '70 - 90', '> 90', NA)) ])
levels_alpha_jeune_DR          <- unique(dr[Indicateur_raccourci %in% "alpha_jeune"                    , factor(Echelle, levels = c('< 50', '50 - 60', '60 - 70','70 - 80', '> 80', NA)) ])
levels_alpha_adulte_DR         <- unique(dr[Indicateur_raccourci %in% "alpha_adulte"                   , factor(Echelle, levels = c('< 50', '50 - 60', '60 - 70','70 - 80', '> 80', NA)) ])
levels_croissance_pib_DR       <- unique(dr[Indicateur_raccourci %in% "croissance_pib"                 , factor(Echelle, levels = c('< 0','0 - 3', '3 - 6', '6 - 9', '> 9', NA))] ) 
levels_pib_hab_DR              <- unique(dr[Indicateur_raccourci %in% "pib_hab"                        , factor(Echelle, levels = c('< 200','200  - 400', '400  - 600', '600  - 1000', '1000 - 1500', '> 1500', NA))] ) 
levels_pib_DR                  <- unique(dr[Indicateur_raccourci %in% "pib"                            , factor(Echelle, levels = c('< 1','1 - 2', '2 - 5', '5 - 15', '15 - 30', '> 30', NA))] ) 
levels_idh_DR                  <- unique(dr[Indicateur_raccourci %in% "idh"                            , factor(Echelle, levels = c('< 0.3','0.4', '0.5', '0.6', '0.7', '> 0.7', NA))] ) 
levels_recette_budg_DR         <- unique(dr[Indicateur_raccourci %in% "recette_budg"                   , factor(Echelle, levels = c('< 10','10 - 15', '15 - 20', '20 - 25', '25 - 30', '> 30', NA))] ) 
levels_pop_pauvrete_DR         <- unique(dr[Indicateur_raccourci %in% "pop_pauvrete"                   , factor(Echelle, levels = c('< 20','20 - 40', '40 - 60', '60 - 80', '> 80', NA))] ) 
levels_mortal_infant_DR        <- unique(dr[Indicateur_raccourci %in% "mortal_infant"                  , factor(Echelle, levels = c('< 0.05','0.05 - 0.10', '0.10 - 0.15', '0.15 - 0.20', '0.20 - 0.25', '> 0.25', NA))] ) 
levels_pop_tot_DR              <- unique(dr[Indicateur_raccourci %in% "pop_tot"                        , factor(Echelle, levels = c('< 100','100 - 200', '200 - 300', '300 - 500', '500 - 1000', '> 1000', NA))] ) 
levels_sous_nutri_DR           <- unique(dr[Indicateur_raccourci %in% "sous_nutri"                     , factor(Echelle, levels = c('< 10','10 - 15', '15 - 20', '20 - 25', '25 - 30', '> 30', NA))] ) 
levels_pop_rurale_DR           <- unique(dr[Indicateur_raccourci %in% "pop_rurale"                     , factor(Echelle, levels = c('< 50','50 - 60', '60 - 70', '70 - 80', '80 - 90','90 - 100',  NA))] ) 
levels_compte_adulte_DR        <- unique(dr[Indicateur_raccourci %in% "compte_adulte"                  , factor(Echelle, levels = c('< 20','20 - 30', '30 - 40', '> 40', NA))] )
levels_compte_jeune_DR         <- unique(dr[Indicateur_raccourci %in% "compte_jeune"                   , factor(Echelle, levels = c('< 20','20 - 30', '30 - 40', '> 40', NA))] ) 
levels_rdt_cerealier_DR        <- unique(dr[Indicateur_raccourci %in% "rdt_cerealier"                  , factor(Echelle, levels = c('< 750','750 - 1000', '1000 - 1250', '1250 - 1500', '1500 - 2000', '> 2000', NA))] )
levels_abo_mobile_DR           <- unique(dr[Indicateur_raccourci %in% "abo_mobile"                     , factor(Echelle, levels = c('< 2','2 - 5', '5 - 10', '10 - 15', '> 15', NA))] ) 
levels_vuln_clim_ferdi_DR      <- unique(dr[Indicateur_raccourci %in% "vuln_clim_ferdi"                , factor(Echelle, levels = c('< 50','50 - 54', '54 - 58', '58 - 62' , '> 62', NA))] )
levels_vuln_clim_ndgain_DR     <- unique(dr[Indicateur_raccourci %in% "vuln_clim_ndgain"               , factor(Echelle, levels = c('< 0.45','0.45 - 0.5', '0.5 - 0.55', '0.55 - 0.6', '> 0.6', NA))] ) 
levels_pop_urbaine_DR          <- unique(dr[Indicateur_raccourci %in% "pop_urbaine"                    , factor(Echelle, levels = c('< 10','10 - 20', '20 - 30', '30 - 40', '40 - 50', '> 50', NA))] ) 
levels_incub_start_up_DR       <- unique(dr[Indicateur_raccourci %in% "incub_start_up"                 , factor(Echelle, levels = c('< 20','20 - 30', '> 30', NA))] ) 
levels_migration_DR            <- unique(dr[Indicateur_raccourci %in% "migration"                      , factor(Echelle, levels = c('< -5','-5 - -1', '-1 - 0', '0 - 1', '> 1', NA))] ) 
#levels_eau_potable_DR          <- unique(dr[Indicateur_raccourci %in% "eau_potable"                    , factor(Echelle, levels = c('< 10','10 - 20', '20 - 40','40 - 60', '> 60', NA))] )
levels_lib_monde_DR            <- unique(dr[Indicateur_raccourci %in% "lib_monde"                      , factor(Echelle, levels = c('Non libre', 'Partiellement libre', 'Libre', NA))] ) 
levels_frag_pol_DR             <- unique(dr[Indicateur_raccourci %in% "frag_pol"                       , factor(Echelle, levels = c('Pas de fragilité politique', 'Fragilité politique', NA) )] ) 
levels_regime_pol_DR           <- unique(dr[Indicateur_raccourci %in% "regime_pol"                     , factor(Echelle, levels = c('Colonie','Autocratie', 'Anocratie fermée', 'Anocratie ouverte', 'Démocratie imparfaite', 'Démocratie', NA))] )



#-----------------------------------                Définitions des paramètres de pyramide des ages et du graphique des évolutions              -------------------------------------------#  ====


liste_age <- c('< 5', '5-10', '10-14','15-19','20-24','25-29', '30-34', '35-39','40-44','45-49','50-54', '55-59','60-64','65-69','70-74','75-79','80-84','85-89','90-94','95-99','100+')

## Légende

l = list(orientation = 'h', xanchor = "center", x = 0.2, y=0.9)


##axes et marge

ay = list(showgrid = F      , 
          title    = ""     ,
          categoryorder = "array",
          categoryarray = liste_age,
          zeroline = FALSE,
          showline = TRUE,
          showticklabels = TRUE,
          linecolor = 'rgb(204, 204, 204)',
          linewidth = 2,
          ticks = 'outside',
          tickcolor = 'rgb(204, 204, 204)',
          tickwidth = 2,
          ticklen = 5,
          tickfont = list(family = 'Arial',
                          size = 11,
                          color = 'rgb(82, 82, 82)'))


xaxis <- list(title = "",
              showline = TRUE,
              showgrid = TRUE,
              linecolor = 'rgb(204, 204, 204)',
              linewidth = 2,
              ticks = 'outside',
              tickcolor = 'rgb(204, 204, 204)',
              tickwidth = 2,
              ticklen = 5,
              tickfont = list(family = 'Arial',
                              size = 11,
                              color = 'rgb(82, 82, 82)'))

yaxis <- list(title          = "Population en millions d'habitants",
              titlefont      = list(family = 'Arial',
                                    size = 11,
                                    color = 'rgb(82, 82, 82)'),
              showgrid       = TRUE,
              zeroline       = FALSE,
              showline       = TRUE,
              showticklabels = TRUE,
              linecolor      = 'rgb(204, 204, 204)',
              linewidth      = 2,
              ticksuffix     = " M",
              ticks          = 'outside',
              tickcolor      = 'rgb(204, 204, 204)',
              tickwidth      = 2,
              ticklen        = 5,
              tickfont       = list(family = 'Arial', size = 11, color = 'rgb(82, 82, 82)'))

margin <- list(l = 50, r = 50, t = 100, b = 25, pad = 4  )

#annotation graphique évolution et pyramide des ages

annotation_pyra     = list(x = 1.0, y = -0.06,
                           text = paste0("© AFD - Agence Française de Développement        Source : UNdata"), 
                           showarrow = F,
                           xref='paper', yref='paper', 
                           xanchor='right', yanchor='auto', 
                           xshift=0, yshift=0,
                           font=list(size=11, color="grey"))


annotation_Evol     = list(x = 1.05, y = -0.16,
                           text = paste0("© AFD - Agence Française de Développement        Source : UNdata"), #"<em>Source : UNData</em>", 
                           showarrow = F,
                           xref='paper', yref='paper', 
                           xanchor='right', yanchor='auto', 
                           xshift=0, yshift=0,
                           font=list(size=10, color="grey") )




# listeIndicAfriquePluriel       =  unique(AFR[Transition == "Afrique au pluriel" & Indicateur != 'PIB PPA']$Indicateur)
# listeIndicAfriqueEnTransitions =  unique(AFR[Transition == "Afrique en transitions"]$Indicateur)
# listeIndicLesTransitions       =  unique(AFR[Transition == "Les transitions"]$Indicateur)
# listeIndicSixDefisARelever     =  unique(AFR[Transition == "Les 6 défis à relever"]$Indicateur)

# listeIndicAfriquePluriel       = list("Économie et développement"   = c( "PIB","PIB par habitant","Croissance du PIB","Recettes budgétaires", 'IDH'),
#                                        "Alphabétisation"             = c("Jeunes" = "Alphabétisation des jeunes", "Adultes" =  "Alphabétisation des adultes" )
# )

# listeIndicAfriqueEnTransitions = list("Population"                  = c( "Population totale" , "Population rurale", "Population vivant dans la pauvreté"),
#                                       "Conséquences de la pauvreté" = c( "Mortalité infantile" , "Sous-nutrition" )
# )

# listeIndicLesTransitions       = list("Agriculture"              = c( "Rendement céréalier", "Eau douce dans l'agriculture" , "Emploi dans l'agriculture" ),
#                                       "Ville"                    = c( 'Bidonvilles',  "Taux d'urbanisation" = "Population urbaine" ),
#                                       "Énergie"                  = c( "Accès à l'électricité" , "Production d'électricité renouvelable"),
#                                       "Compte et abonnement"     = c( "Abonnements mobiles" , "Titulaire d'un compte, jeunes adultes", "Titulaire d'un compte, adultes" ),
#                                       "Vulnérabilité climatique" = c( "Ndgain" = "Vulnérabilité climatique Ndgain" , "Ferdi" =  "Vulnérabilité climatique FERDI"),
#                                       "Politique et liberté"     = c( "Liberté dans le monde" , "Fragilité politique", "Régimes politiques")
# )

# listeIndicSixDefisARelever = list("Autres"             = c( "Migration", "Incubateurs de start-up"  ),
#                                   "Écoles primaires"   = c("Eau potable" = "Eau potable - Écoles primaires",  "Électricité"="Accès électricité - Écoles primaires", "Sanitaires non mixtes" = "Installations sanitaires non mixtes - Écoles primaires"),
#                                   "Enseignants formés" = c("Primaire" = "Enseignants formés - primaire", "Secondaire" = "Enseignants formés - secondaire"),
#                                   "Import / Export"    = c("Volume Import - biens","Volume Export - biens","Volume Import - biens et services","Volume Export - biens et services")
# )


