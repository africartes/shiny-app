
source(local = TRUE, "src/observe/observeAfr.Six.Defis.R", encoding = "UTF-8")


######################################
#       Création de la carte 1       #
######################################


output$carteAfrSixDefis <- renderLeaflet({

  carte_vision_pays_dr(   condition      = condition7(), 
                          listeAnnee1    = listeAnnee4(), 
                          listeAnnee2    = listeAnneeDR4(), 
                          base1          = AFR, 
                          base2          = dr, 
                          fond_de_carte1 = africa,
                          fond_de_carte2 = africaDR, 
                          indic          = input$afr_SixDefis,
                          choixAnnee     = input$choixAnneeAfrSixDefis   )
})


######################################
#       Création de la carte 2       #
######################################


output$carteAfrSixDefis2 <- renderLeaflet({

  carte_vision_pays_dr(   condition      = condition8(), 
                          listeAnnee1    = listeAnnee4(), 
                          listeAnnee2    = listeAnneeDR4(), 
                          base1          = AFR, 
                          base2          = dr, 
                          fond_de_carte1 = africa,
                          fond_de_carte2 = africaDR, 
                          indic          = input$afr_SixDefis,
                          choixAnnee     = input$choixAnneeAfrSixDefis2   )
})


#######################################
#           Vision Mondiale           #
#######################################


output$carte_globAfrSixDefis <- renderLeaflet({

  carte_vision_monde(    condition     = input$derDispoAfrSixDefis,
                         listeAnnee    = listeAnneeGlob4(),
                         base          = base_mondiale,
                         fond_de_carte = MONDE,
                         indic         = input$afr_SixDefis,
                         choixAnnee    = input$selectAnneeGlobAfrSixDefis,
                         base_corres   = correspondance          )
})